@echo off

set code_dir=%~dp0
set build_dir="..\build\"

pushd %code_dir%

if not exist %build_dir% mkdir %build_dir%

pushd %build_dir%

rem TODO: Need to go over the warning and clean up. Especiallythe cast align and bad function part
rem which seems like could lead to undefined/bad behavior
rem
if "%1"=="clang" goto compile_clang

set msvc_d_warnings=/wd4061
set msvc_d_warnings=%msvc_d_warnings% /wd4242
set msvc_d_warnings=%msvc_d_warnings% /wd4244
set msvc_d_warnings=%msvc_d_warnings% /wd4018
set msvc_d_warnings=%msvc_d_warnings% /wd4820
set msvc_d_warnings=%msvc_d_warnings% /wd4100
set msvc_d_warnings=%msvc_d_warnings% /wd4710
set msvc_d_warnings=%msvc_d_warnings% /wd4189
set msvc_d_warnings=%msvc_d_warnings% /wd5045
cl /std:c11 /TC /Wall /nologo /Od /Zi /WX %msvc_d_warnings% %code_dir%png_decoder.c /link User32.lib Gdi32.lib

goto compile_finished

:compile_clang

set clang_d_warnings=-Wno-sign-conversion
set clang_d_warnings=%clang_d_warnings% -Wno-shorten-64-to-32
set clang_d_warnings=%clang_d_warnings% -Wno-unused-parameter
set clang_d_warnings=%clang_d_warnings% -Wno-implicit-int-conversion
set clang_d_warnings=%clang_d_warnings% -Wno-sign-compare
set clang_d_warnings=%clang_d_warnings% -Wno-bad-function-cast
set clang_d_warnings=%clang_d_warnings% -Wno-unused-function
set clang_d_warnings=%clang_d_warnings% -Wno-switch-enum
set clang_d_warnings=%clang_d_warnings% -Wno-switch-enum
set clang_d_warnings=%clang_d_warnings% -Wno-unused-variable
set clang_d_warnings=%clang_d_warnings% -Wno-cast-align

clang -Weverything -std=c11 %clang_d_warnings% -o png_decoder.exe -l User32.lib -l Gdi32.lib %code_dir%png_decoder.c

:compile_finished

popd
popd

