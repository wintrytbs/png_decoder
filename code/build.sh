#!/bin/bash

if [ ! -d "../build" ]; then
    mkdir "../build"
fi

pushd "../build" > /dev/null
clang -o png_decoder -Wall -g -Wno-unused-function -Wno-unused-variable ../code/png_decoder_linux.c
popd > /dev/null