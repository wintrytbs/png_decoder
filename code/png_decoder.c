#if defined(__linux__)

#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>

typedef struct stat file_stat;

#elif defined(_WIN64)

#if defined(_MSC_VER)
#pragma warning (push, 0)
#elif defined(__clang__)
#pragma GCC diagnostics push
#pragma GCC diagnostics ignored "-Weverything"
#endif
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#else

#error Unsupported OS Detected

#endif

#include <memory.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

#if defined(_MSC_VER)
#pragma warning(pop)
#elif defined(__clang__)
#pragma GCC diagnostics pop
#endif

#define tbs_assert(cond) assert(cond)
#define ARRAY_COUNT(arr) (sizeof(arr) / sizeof(arr[0]))

#if 0
#if defined(__clang__)
__attribute__((__format__(__printf__, 1, 2)))
#endif
static void PrintDebugF(char *text, ...)
{
    va_list argList;
    va_start(argList, text);
    vprintf(text, argList);
    va_end(argList);
}
#else
#define PrintDebugF(text, ...)
#define PrintDebugText(text)
#endif

#if defined(__clang__)
__attribute__((__format__(__printf__, 1, 2)))
#endif
static void PrintTextF(char *text, ...)
{
    va_list argList;
    va_start(argList, text);
    vprintf(text, argList);
    va_end(argList);
}

typedef int8_t      i8;
typedef uint8_t     u8;
typedef int16_t     i16;
typedef uint16_t    u16;
typedef int32_t     i32;
typedef uint32_t    u32;
typedef int64_t     i64;
typedef uint64_t    u64;

typedef enum chunk_type
{
    // Critical chunks
    ChunkType_None,
    ChunkType_IHDR,
    ChunkType_PLTE,
    ChunkType_IDAT,
    ChunkType_IEND,

    // Ancillary chunks
    ChunkType_cHRM,
    ChunkType_gAMA,
    ChunkType_iCCP,
    ChunkType_sBIT,
    ChunkType_sRGB,
    ChunkType_bKGD,
    ChunkType_hIST,
    ChunkType_tRNS,
    ChunkType_pHYs,
    ChunkType_sPLT,
    ChunkType_tIME,
    ChunkType_iTXt,
    ChunkType_tEXt,
    ChunkType_zTXt,
} chunk_type;

typedef enum deflate_block_type
{
    DeflateBlockType_NoCompress,
    DeflateBlockType_Fixed,
    DeflateBlockType_Dynamic,
    DeflateBlockType_Reserved,
} block_type;


typedef struct file_data
{
    u8 *data;
    u64 size;
} file_data;


typedef struct byte_stream
{
    u8 *base;
    u8 *offset;
    u64 size;
} byte_stream;

typedef struct bit_stream
{
    u8 *base;
    u8 *offset;
    u8 bitIndex;
    u64 size;
} bit_stream;

typedef struct chunk_header
{
    chunk_type type;
    byte_stream data;
    u32 crc;
} chunk_header;

typedef struct ihdr_chunk
{
    u32 width;
    u32 height;
    i8 bitDepth;
    i8 colorType;
    i8 compressMethod;
    i8 filterMethod;
    i8 interlaceMethod;
} ihdr_chunk;

typedef struct idat_chunk
{
    u8 CM;
    u8 FLG;
    u8 FCHECK;
    u8 FDICT;
    u8 FLEVEL;
    bit_stream compressedData;
} idat_chunk;


#define DEF_IHDR ((((u32)'I') << 0)   | \
                  (((u32)'H') << 8)   | \
                  (((u32)'D') << 16)  | \
                  (((u32)'R') << 24))

#define DEF_PLTE ((((u32)'P') << 0)   | \
                  (((u32)'L') << 8)   | \
                  (((u32)'T') << 16)  | \
                  (((u32)'E') << 24))

#define DEF_IDAT ((((u32)'I') << 0)   | \
                  (((u32)'D') << 8)   | \
                  (((u32)'A') << 16)  | \
                  (((u32)'T') << 24))

#define DEF_IEND ((((u32)'I') << 0)   | \
                  (((u32)'E') << 8)   | \
                  (((u32)'N') << 16)  | \
                  (((u32)'D') << 24))


#define DEF_cHRM ((((u32)'c') << 0)   | \
                  (((u32)'H') << 8)   | \
                  (((u32)'R') << 16)  | \
                  (((u32)'M') << 24))

#define DEF_gAMA ((((u32)'g') << 0)   | \
                  (((u32)'A') << 8)   | \
                  (((u32)'M') << 16)  | \
                  (((u32)'A') << 24))

#define DEF_iCCP ((((u32)'i') << 0)   | \
                  (((u32)'C') << 8)   | \
                  (((u32)'C') << 16)  | \
                  (((u32)'P') << 24))

#define DEF_sBIT ((((u32)'s') << 0)   | \
                  (((u32)'B') << 8)   | \
                  (((u32)'I') << 16)  | \
                  (((u32)'T') << 24))

#define DEF_sRGB ((((u32)'s') << 0)   | \
                  (((u32)'R') << 8)   | \
                  (((u32)'G') << 16)  | \
                  (((u32)'B') << 24))

#define DEF_bKGD ((((u32)'b') << 0)   | \
                  (((u32)'K') << 8)   | \
                  (((u32)'G') << 16)  | \
                  (((u32)'D') << 24))

#define DEF_hIST ((((u32)'h') << 0)   | \
                  (((u32)'I') << 8)   | \
                  (((u32)'S') << 16)  | \
                  (((u32)'T') << 24))

#define DEF_tRNS ((((u32)'t') << 0)   | \
                  (((u32)'R') << 8)   | \
                  (((u32)'N') << 16)  | \
                  (((u32)'S') << 24))

#define DEF_pHYs ((((u32)'p') << 0)   | \
                  (((u32)'H') << 8)   | \
                  (((u32)'Y') << 16)  | \
                  (((u32)'s') << 24))

#define DEF_sPLT ((((u32)'s') << 0)   | \
                  (((u32)'P') << 8)   | \
                  (((u32)'L') << 16)  | \
                  (((u32)'T') << 24))

#define DEF_tIME ((((u32)'t') << 0)   | \
                  (((u32)'I') << 8)   | \
                  (((u32)'M') << 16)  | \
                  (((u32)'E') << 24))

#define DEF_iTXt ((((u32)'i') << 0)   | \
                  (((u32)'T') << 8)   | \
                  (((u32)'X') << 16)  | \
                  (((u32)'t') << 24))

#define DEF_tEXt ((((u32)'t') << 0)   | \
                  (((u32)'E') << 8)   | \
                  (((u32)'X') << 16)  | \
                  (((u32)'t') << 24))

#define DEF_zTXt ((((u32)'z') << 0)   | \
                  (((u32)'T') << 8)   | \
                  (((u32)'X') << 16)  | \
                  (((u32)'t') << 24))

typedef struct code_lookup
{
    u32 baseValue;
    u32 extraBits;
} code_lookup;

static code_lookup lengthLookup[] =
{
    {.baseValue = 3,   .extraBits = 0},
    {.baseValue = 4,   .extraBits = 0},
    {.baseValue = 5,   .extraBits = 0},
    {.baseValue = 6,   .extraBits = 0},
    {.baseValue = 7,   .extraBits = 0},
    {.baseValue = 8,   .extraBits = 0},
    {.baseValue = 9,   .extraBits = 0},
    {.baseValue = 10,  .extraBits = 0},
    {.baseValue = 11,  .extraBits = 1},
    {.baseValue = 13,  .extraBits = 1},
    {.baseValue = 15,  .extraBits = 1},
    {.baseValue = 17,  .extraBits = 1},
    {.baseValue = 19,  .extraBits = 2},
    {.baseValue = 23,  .extraBits = 2},
    {.baseValue = 27,  .extraBits = 2},
    {.baseValue = 31,  .extraBits = 2},
    {.baseValue = 35,  .extraBits = 3},
    {.baseValue = 43,  .extraBits = 3},
    {.baseValue = 51,  .extraBits = 3},
    {.baseValue = 59,  .extraBits = 3},
    {.baseValue = 67,  .extraBits = 4},
    {.baseValue = 83,  .extraBits = 4},
    {.baseValue = 99,  .extraBits = 4},
    {.baseValue = 115, .extraBits = 4},
    {.baseValue = 131, .extraBits = 5},
    {.baseValue = 163, .extraBits = 5},
    {.baseValue = 195, .extraBits = 5},
    {.baseValue = 227, .extraBits = 5},
    {.baseValue = 258, .extraBits = 0},
};

static code_lookup distanceLookup[] =
{
    {.baseValue = 1,     .extraBits = 0},
    {.baseValue = 2,     .extraBits = 0},
    {.baseValue = 3,     .extraBits = 0},
    {.baseValue = 4,     .extraBits = 0},
    {.baseValue = 5,     .extraBits = 1},
    {.baseValue = 7,     .extraBits = 1},
    {.baseValue = 9,     .extraBits = 2},
    {.baseValue = 13,    .extraBits = 2},
    {.baseValue = 17,    .extraBits = 3},
    {.baseValue = 25,    .extraBits = 3},
    {.baseValue = 33,    .extraBits = 4},
    {.baseValue = 49,    .extraBits = 4},
    {.baseValue = 65,    .extraBits = 5},
    {.baseValue = 97,    .extraBits = 5},
    {.baseValue = 129,   .extraBits = 6},
    {.baseValue = 193,   .extraBits = 6},
    {.baseValue = 257,   .extraBits = 7},
    {.baseValue = 385,   .extraBits = 7},
    {.baseValue = 513,   .extraBits = 8},
    {.baseValue = 769,   .extraBits = 8},
    {.baseValue = 1025,  .extraBits = 9},
    {.baseValue = 1537,  .extraBits = 9},
    {.baseValue = 2049,  .extraBits = 10},
    {.baseValue = 3073,  .extraBits = 10},
    {.baseValue = 4097,  .extraBits = 11},
    {.baseValue = 6145,  .extraBits = 11},
    {.baseValue = 8193,  .extraBits = 12},
    {.baseValue = 12289, .extraBits = 12},
    {.baseValue = 16385, .extraBits = 13},
    {.baseValue = 24577, .extraBits = 13},
};

static void PrintByteArray(const char *name, u8 *buff, u32 count)
{
    printf("%s:", name);
    for(u32 i = 0; i < count; ++i)
    {
        printf(" %hhu", buff[i]);
    }

    printf("\n");
}

static void PrintBitValue(const char *name, u8 *buff, u32 count)
{
    printf("Bit Stream: ");
    for(u32 i = 0; i < count; ++i)
    {
        u8 value = buff[i];
        for(u32 b = 0; b < 8; ++b)
        {
            u32 bit = 7 - b;
            printf("%u", (value >> bit) & 1);
        }

        printf(" ");
    }

    printf("\n");
#if 0
    printf("%s:", name);
    for(u32 i = 0; i < count; ++i)
    {
        u8 value;
        value >> 7
        for(u32 b = 0; b < 8; ++b)
        {

        }
    }
#endif
}

static void PrintChunkHeaderByteStream(u8 *buff)
{
    PrintByteArray("Chunk Length", buff, 4);
    PrintByteArray("Chunk Type", buff + 4, 4);

    u32 l = *((u32 *)(buff));
    l = ((l & 0xFF000000) >> 24) | 
        ((l & 0x00FF0000) >> 8) | 
        ((l & 0x0000FF00) << 8) |
        ((l & 0x000000FF) << 24);

    if(l > 0)
    {
        printf("Length = %u\n", l);
        PrintByteArray("Chunk Data", buff + 8, l); 
    }

    PrintByteArray("CRC", buff + 8 + l, 4);
}

static inline u32 BigToLittleEndianU32(u32 value)
{
    u32 result = ((value & 0xFF000000) >> 24) |
                 ((value & 0x00FF0000) >> 8)  |
                 ((value & 0x0000FF00) << 8)  |
                 ((value & 0x000000FF) << 24);

    return result;
}

static inline u32 Stream_GetU32AndAdvance(byte_stream *stream)
{
    tbs_assert(stream->offset + 4u <= stream->base + stream->size);
    u32 result = *((u32 *)(stream->offset));
    stream->offset += sizeof(u32);
    return result;
}

static chunk_header Stream_GetNextChunkAndAdvance(byte_stream *stream)
{
    // TODO(torgrim): Just make a direct mapping between type and enum so that
    // we don't have to have a switch when getting the chunk and one when
    // processing the data. Then we can just write the type directly to the chunk struct
    chunk_header result = {0};
    u32 length = Stream_GetU32AndAdvance(stream);
    u32 typeDef = Stream_GetU32AndAdvance(stream);
    switch(typeDef)
    {
        case DEF_IHDR:
        {
            result.type = ChunkType_IHDR;
            printf("Got IHDR chunk\n");
        }break;
        case DEF_PLTE:
        {
            result.type = ChunkType_PLTE;
            printf("Got PLTE chunk\n");
        }break;
        case DEF_IDAT:
        {
            result.type = ChunkType_IDAT;
            printf("Got IDAT chunk\n");
        }break;
        case DEF_cHRM:
        {
            result.type = ChunkType_cHRM;
            printf("Got cHRM chunk\n");
        }break;
        case DEF_gAMA:
        {
            result.type = ChunkType_gAMA;
            printf("Got gAMA chunk\n");
        }break;
        case DEF_iCCP:
        {
            result.type = ChunkType_iCCP;
            printf("Got iCCP chunk\n");
        }break;
        case DEF_sBIT:
        {
            result.type = ChunkType_sBIT;
            printf("Got sBIT chunk\n");
        }break;
        case DEF_sRGB:
        {
            result.type = ChunkType_sRGB;
            printf("Got sRGB chunk\n");
        }break;
        case DEF_bKGD:
        {
            result.type = ChunkType_bKGD;
            printf("Got bKGD chunk\n");
        }break;
        case DEF_hIST:
        {
            result.type = ChunkType_hIST;
            printf("Got hIST chunk\n");
        }break;
        case DEF_tRNS:
        {
            result.type = ChunkType_tRNS;
            printf("Got tRNS chunk\n");
        }break;
        case DEF_pHYs:
        {
            result.type = ChunkType_pHYs;
            printf("Got pHYs chunk\n");
        }break;
        case DEF_sPLT:
        {
            result.type = ChunkType_sPLT;
            printf("Got sPLT chunk\n");
        }break;
        case DEF_tIME:
        {
            result.type = ChunkType_tIME;
            printf("Got tIME chunk\n");
        }break;
        case DEF_iTXt:
        {
            result.type = ChunkType_iTXt;
            printf("Got iTXt chunk\n");
        }break;
        case DEF_tEXt:
        {
            result.type = ChunkType_tEXt;
            printf("Got tEXt chunk\n");
        }break;
        case DEF_zTXt:
        {
            result.type = ChunkType_zTXt;
            printf("Got zTXt chunk\n");
        }break;
        case DEF_IEND:
        {
            result.type = ChunkType_IEND;
            printf("Got IEND chunk\n");
        }break;
        default:
        {
            printf("Unsupported chunk found\n");
        }
    }

    result.data.base = stream->offset;
    result.data.offset = result.data.base;
    result.data.size = BigToLittleEndianU32(length);
    stream->offset += result.data.size;
    result.crc = Stream_GetU32AndAdvance(stream);

    return result;
}

static inline u8 ByteStream_GetU8AndAdvance(byte_stream *s)
{
    tbs_assert(s->offset + sizeof(u8) <= s->base + s->size);
    u8 result = *s->offset;
    s->offset += sizeof(u8);
    return result;
}

static inline u8 Chunk_GetI8AndAdvance(chunk_header *chunk)
{
    tbs_assert(chunk->data.offset + sizeof(i8) <= chunk->data.base + chunk->data.size);
    i8 result = *chunk->data.offset;
    chunk->data.offset += sizeof(i8);
    return result;
}

static inline u8 Chunk_GetU8AndAdvance(chunk_header *chunk)
{
    tbs_assert(chunk->data.offset + sizeof(u8) <= chunk->data.base + chunk->data.size);
    u8 result = *chunk->data.offset;
    chunk->data.offset += sizeof(u8);
    return result;
}

static inline u32 Chunk_GetU32AndAdvance(chunk_header *chunk)
{
    tbs_assert(chunk->data.offset + sizeof(u32) <= chunk->data.base + chunk->data.size);
    u32 result = *(u32 *)chunk->data.offset;
    chunk->data.offset += sizeof(u32);
    return result;
}

static void PrintBits(const char *text, u32 bits, u32 count)
{
    printf("%s: ", text);
    tbs_assert(count <= 32);
    for(u32 i = 0; i < count; ++i)
    {
        printf("%u", (bits >> ((count-1) - i)) & 1);
    }
    printf("\n");
}

static u32 EatBitsLSB(bit_stream *s, u32 count)
{
    tbs_assert(count <= 32);
    tbs_assert(s->bitIndex < 8);
    u32 result = 0;
    for(u32 i = 0; i < count; ++i)
    {
        u8 value = *s->offset;
        result |= ((value >> s->bitIndex) & 1) << i;
        ++s->bitIndex;
        if(s->bitIndex == 8)
        {
            s->bitIndex = 0;
            ++s->offset;
        }
    }

    return result;
}

static void BitStream_PrintBits(const char *text, bit_stream *s, u32 count)
{
    bit_stream orgStream = *s;
    printf("%s: ", text);
    for(u32 i = 0; i < count; ++i)
    {
        u32 bit = EatBitsLSB(s, 1);
        printf("%u", bit & 1);
    }

    printf("\n");

    *s = orgStream;
}

static void ProcessNonCompressedBlock(bit_stream *inStream, byte_stream *outStream)
{
    u16 len = *(u16 *)inStream->offset;
    inStream->offset += sizeof(u16);
    u16 nlen = *(u16 *)inStream->offset;
    inStream->offset += sizeof(u16);
    PrintDebugF("Non-compressed Block Length: %u\n", len);
    memcpy(outStream->offset, inStream->offset, len);
    inStream->offset += len;
    outStream->offset += len;
}

static void DecompressFixedHuffman(bit_stream *inStream, byte_stream *outStream)
{
    while(true)
    {
        u16 code = 0;

        tbs_assert(inStream->bitIndex < 8);
        u32 bi = inStream->bitIndex;
        u16 bits = *((u16 *)inStream->offset);
        bits >>= bi;

        code = 0;
        code = (bits & 1u);
        for(u32 i = 1; i < 9; ++i)
        {
            code = ((bits >> i) & 1u) | (code << 1);
        }

        u32 litLo = code >> 1;
        u32 litHi = code;
        u32 lenLo = code >> 2;
        u32 lenHi = litLo;
        bool parseDist = false;
        u32 value = 0;
        if(litLo >= 48 && litLo <= 191)
        {
            PrintDebugText("Low literal value code found\n");
            value = litLo - 48;
            code = litLo;
            PrintDebugF("Actual Value: %u\n", value);
            bi += 8;
            *outStream->offset = value;
            ++outStream->offset;
        }
        else if(litHi >= 400 && litHi <= 511)
        {
            PrintDebugText("Hight literal value code found\n");
            value = 144 + (litHi - 400);
            code = litHi;
            bi += 9;
            *outStream->offset = value;
            ++outStream->offset;
        }
        else if(lenLo <= 23)
        {
            PrintDebugText("Low length value code found\n");
            code = lenLo;
            bi += 7;
            parseDist = true;
            // NOTE(torgrim): Since this code starts at 256 but our
            // code lookup is only for code >= 257 we need to offset it by one.
            value = 256 + lenLo;
        }
        else if(lenHi >= 192 && lenHi <= 199)
        {
            PrintDebugText("High length value code found\n");
            code = lenHi;
            bi += 8;
            parseDist = true;
            value = 280 + (lenHi - 192);
        }
        else
        {
            PrintDebugText("FIXED HUFFMAN::ERROR:: Invalid Huffman code found\n");
        }

        if(bi >= 8)
        {
            inStream->offset += (bi / 8);
            bi %= 8;
        }

        //PrintBits("Code", code, 9);
        inStream->bitIndex = bi;

        if(value == 256)
        {
            break;
        }

        if(parseDist && code != 0)
        {
            // TODO(torgrim): Does lenCode = 256(stream end) actually have a dist value
            // or is it just the len value?

            u32 len = 0;
            u32 dis = 0;
            tbs_assert(value >= 257);
            if((value - 257) >= ARRAY_COUNT(lengthLookup))
            {
                PrintDebugF("Length Value: %u\n", value);
                tbs_assert(false);
            }
            code_lookup lenInfo = lengthLookup[value - 257];
            PrintDebugF("Length Base: %u, Extra Bits: %u\n", lenInfo.baseValue, lenInfo.extraBits);
            len = lenInfo.baseValue;

            u32 offsetValue = 0;
            for(u32 ei = 0; ei < lenInfo.extraBits; ++ei)
            {
                u8 bit = EatBitsLSB(inStream, 1);
                offsetValue |= (bit << ei);
            }

            len += offsetValue;
            PrintDebugF("Actual Length: %u\n", len);

            u8 distCode = 0;
            for(u32 i = 0; i < 5; ++i)
            {
                u8 bit = EatBitsLSB(inStream, 1);
                distCode |= (bit << (4-i));
            }

            PrintDebugF("Distance Code: %hhu\n", distCode);
            tbs_assert(distCode < ARRAY_COUNT(distanceLookup));
            code_lookup disInfo = distanceLookup[distCode];
            PrintDebugF("Distance Base: %u, Extra Bits: %u\n", disInfo.baseValue, disInfo.extraBits);

            dis = disInfo.baseValue;
            offsetValue = 0;
            for(u32 ei = 0; ei < disInfo.extraBits; ++ei)
            {
                u8 bit = EatBitsLSB(inStream, 1);
                offsetValue |= (bit << ei);
            }

            dis += offsetValue;
            for(u32 i = len; i > 0; --i)
            {
                *outStream->offset = *(outStream->offset - dis);
                ++outStream->offset;
            }
        }
    }
}


typedef enum node_type
{
    NodeType_None,
    NodeType_Root,
    NodeType_Leaf,
    NodeType_Bit,
} node_type;

typedef struct huff_tree_node
{
    node_type type;
    struct huff_tree_node *children[2];
    u32 value;
} huff_tree_node;

static u32 codeLengthMap[] =
{
    16,
    17,
    18,
    0,
    8,
    7,
    9,
    6,
    10,
    5,
    11,
    4,
    12,
    3,
    13,
    2,
    14,
    1,
    15,
};

typedef struct mem_pool
{
    u8 *base;
    u32 count;
    u32 capacity;
    size_t elementSize;
} mem_pool;

#define Pool_Create(type, capacity) Pool_Create_Internal(sizeof(type), capacity)
static mem_pool Pool_Create_Internal(size_t elementSize, u32 capacity)
{
    mem_pool result;
    result.base = malloc(elementSize * capacity);
    memset(result.base, 0, elementSize * capacity);
    result.count = 0;
    result.capacity = capacity;
    result.elementSize = elementSize;

    return result;
}

static void *Pool_AllocUnit(mem_pool *p)
{
    void *result = NULL;
    if(p->count < p->capacity)
    {
        result = p->base + (p->count * p->elementSize);
        p->count++;
    }
    else
    {
        PrintTextF("POOL ERROR:: Could not allocate new unit\n");
        tbs_assert(false);
    }

    tbs_assert(result != NULL);
    return result;
}

static huff_tree_node *CodeLenCodes_CreateHuffmanTree(bit_stream *s, u32 readCount, mem_pool *nodePool)
{
    PrintDebugF("Code Length Count: %u\n", readCount);
    u32 codeLengthFreq[8] = {0};
    u8 *oldOffset = s->offset;
    u32 oldBitIndex = s->bitIndex;
    PrintDebugText("\n");
    u8 testVal = *s->offset;
    testVal >>= s->bitIndex;
    for(u32 i = 0; i < 8 - oldBitIndex; ++i)
    {
        PrintDebugF("%u", (testVal >> i) & 1);
    }
    for(i32 j = 1; j < 8; ++j)
    {
        testVal = *(s->offset + j);
        for(u32 i = 0; i < 8; ++i)
        {
            PrintDebugF("%u", (testVal >> i) & 1);
        }

    }

    PrintDebugText("\n");
    for(u32 i = 0; i < 64 - oldBitIndex; ++i)
    {
        u8 bit = EatBitsLSB(s, 1);
        PrintDebugF("%u", bit & 1);
    }
    s->offset = oldOffset;
    s->bitIndex = oldBitIndex;

    PrintDebugText("\n");

    u16 codeLengths[ARRAY_COUNT(codeLengthMap)] = {0};
    for(u32 i = 0; i < readCount; ++i)
    {
        u32 trueIndex = codeLengthMap[i];
        u16 codeLength = EatBitsLSB(s, 3);
        tbs_assert(codeLength < ARRAY_COUNT(codeLengthMap));
        codeLengths[trueIndex] = codeLength;
        PrintDebugF("Value: %u, Length: %u\n", codeLengthMap[i], codeLengths[trueIndex]);
        codeLengthFreq[codeLength]++;
    }

    codeLengthFreq[0] = 0;
    u32 nextValue[8] = {0};
    u32 prevBaseValue = 0;
    u32 prevCount = 0;
    for(u32 i = 1;i <= 7; ++i)
    {
        PrintDebugF("Length: %u, Count: %u\n", i, codeLengthFreq[i]);
        u32 baseValue = (prevBaseValue + prevCount) << 1;
        prevBaseValue = baseValue;
        prevCount = codeLengthFreq[i];
        nextValue[i] = baseValue;
    }

    for(u32 i = 1; i <= 7; ++i)
    {
        PrintDebugF("Bit Length: %u, Base: %u\n", i, nextValue[i]);
    }

    u32 codes[19] = {0};

    for(u32 i = 0; i < 19; ++i)
    {
        u32 cl = codeLengths[i];
        if(cl > 0)
        {
            // TODO(torgrim): Not sure I like that this is changing
            // the input list. If this should be the case, make it more explicit to the caller
            // that the list is modified.
            codes[i] = nextValue[cl];
            nextValue[cl]++;
        }
    }

    huff_tree_node *root = Pool_AllocUnit(nodePool);
    for(u32 i = 0; i < 19; ++i)
    {
        u16 len = codeLengths[i];
        u32 code = codes[i];
        PrintDebugF("Value: %u, Length: %u, ", i, len);
        //PrintBits("Code", code, len);

        if(len > 0)
        {
            huff_tree_node *prevNode = root;
            for(u32 j = 0; j < len-1; ++j)
            {
                u32 childIndex = (code >> ((len - 1) - j)) & 1;
                if(prevNode->children[childIndex] == NULL)
                {
                    huff_tree_node *newNode = Pool_AllocUnit(nodePool);
                    newNode->type = NodeType_Bit;
                    prevNode->children[childIndex] = newNode;
                }

                prevNode = prevNode->children[childIndex];
            }


            u32 leafIndex = code & 1;
            huff_tree_node *leafNode = Pool_AllocUnit(nodePool);

            leafNode->type = NodeType_Leaf;
            leafNode->value = i;

            tbs_assert(prevNode->children[leafIndex] == NULL);
            prevNode->children[leafIndex] = leafNode;
        }
    }

    return root;
}

static huff_tree_node *CreateHuffCodesLitLenDist(u32 *list, u32 count, mem_pool *nodePool)
{
    u32 codeLengthFreq[16] = {0};
    for(u32 i = 0; i < count; ++i)
    {
        tbs_assert(list[i] < 16);
        codeLengthFreq[list[i]]++;
    }

    codeLengthFreq[0] = 0;

    u32 prevBaseValue = 0;
    u32 prevCount = 0;
    u32 nextValue[16] = {0};
    for(u32 i = 1; i <= 15; ++i)
    {
        PrintDebugF("Length: %u, Count: %u\n", i, codeLengthFreq[i]);
        u32 baseValue = (prevBaseValue + prevCount) << 1;
        prevBaseValue = baseValue;
        prevCount = codeLengthFreq[i];
        nextValue[i] = baseValue;
    }

    huff_tree_node *root = Pool_AllocUnit(nodePool);
    for(u32 i = 0; i < count; ++i)
    {
        u32 len = list[i];
        u32 code = nextValue[len];
        nextValue[len]++;
        if(len > 0)
        {
            tbs_assert(codeLengthFreq[len] > 0);
            PrintDebugF("Index %u, ", i);
            //PrintBits("Code", code, len);

            huff_tree_node *prevNode = root;
            for(u32 j = 0; j < len-1; ++j)
            {
                u32 childIndex = (code >> ((len - 1) - j)) & 1;
                if(prevNode->children[childIndex] == NULL)
                {
                    huff_tree_node *newNode = Pool_AllocUnit(nodePool);
                    newNode->type = NodeType_Bit;
                    prevNode->children[childIndex] = newNode;
                }

                prevNode = prevNode->children[childIndex];
            }


            u32 leafIndex = code & 1;
            huff_tree_node *leafNode = Pool_AllocUnit(nodePool);

            leafNode->type = NodeType_Leaf;
            leafNode->value = i;

            tbs_assert(prevNode->children[leafIndex] == NULL);
            prevNode->children[leafIndex] = leafNode;
        }
    }

    return root;
}

static void CodeLenCodes_DecodeHuffmanStream(bit_stream *s, huff_tree_node *rootNode, u32 count, u32 *result)
{
    huff_tree_node *node = rootNode;
    u32 resultIndex = 0;
    u32 dataIndex = 0;
    while(dataIndex < count)
    {
        PrintDebugText("Bits: ");
        while(node != NULL && node->type != NodeType_Leaf)
        {
            u8 nextBit = EatBitsLSB(s, 1);
            PrintDebugF("%u", nextBit);
            node = node->children[nextBit];
        }

        PrintDebugText(" ");
        tbs_assert(node != NULL);
        if(node->type == NodeType_Leaf)
        {
            if(node->value < 16)
            {
                PrintDebugText("Got Code Length < 16\n");
                PrintDebugF("Data Index: %u, Value: %u\n", dataIndex, node->value);
                ++dataIndex;
                result[resultIndex++] = node->value;
            }
            else if(node->value == 16)
            {
                PrintDebugText("====Got Code Length = 16 ====\n");
                u16 length = EatBitsLSB(s, 2) + 3;
                PrintDebugF("Repeat length: %u\n", length);
                tbs_assert(resultIndex > 0);

                // TODO(torgrim): Just use memset?
                u32 value = result[resultIndex-1];
                for(u32 i = 0; i < length; ++i)
                {
                    result[resultIndex++] = value;
                }
                dataIndex += length;
            }
            else if(node->value == 17)
            {
                PrintDebugText("Got Code Length = 17\n");
                u16 length = EatBitsLSB(s, 3) + 3;
                PrintDebugF("Repeat Length = %u\n", length);
                // TODO(torgrim): Just use memset?
                for(u32 i = 0; i < length; ++i)
                {
                    result[resultIndex++] = 0;
                }
                dataIndex += length;
            }
            else if(node->value == 18)
            {
                PrintDebugText("Got Code Length = 18\n");
                u16 length = EatBitsLSB(s, 7) + 11;
                PrintDebugF("Repeat Length = %u\n", length);

                // TODO(torgrim): Just use memset?
                for(u32 i = 0; i < length; ++i)
                {
                    result[resultIndex++] = 0;
                }
                dataIndex += length;
            }
            else
            {
                tbs_assert(false);
            }
        }
        else
        {
            tbs_assert(false);
        }

        node = rootNode;
    }

    //CreateHuffmanTree(litLenAlphabet, litCodeLengthCount);
    PrintDebugF("Index on end: %u\n", resultIndex);
    PrintDebugF("Should be below or equal: %u\n", count);
    tbs_assert(resultIndex <= count);
}


static void DecompressDynamicHuffman(bit_stream *inStream, byte_stream *outStream)
{
    u32 HLIT = EatBitsLSB(inStream, 5);
    u32 HDIST = EatBitsLSB(inStream, 5);
    u32 HCLEN = EatBitsLSB(inStream, 4);

    PrintDebugF("HLIT: %u\n", HLIT);
    PrintDebugF("HDIST: %u\n", HDIST);
    PrintDebugF("HCLEN: %u\n", HCLEN);

    u32 actualCodeCount = HCLEN+4;
    mem_pool codeLenCodesNodePool = Pool_Create(huff_tree_node, (1 << 8) - 1);
    tbs_assert(actualCodeCount <= ARRAY_COUNT(codeLengthMap));
    huff_tree_node *root = CodeLenCodes_CreateHuffmanTree(inStream, actualCodeCount, &codeLenCodesNodePool);
    PrintDebugText("\n\nProcessing Lit/length codes...\n\n");

    u32 litCodeLengthCount = HLIT + 257;
    u32 *litLengths = malloc(sizeof(u32) * litCodeLengthCount);
    CodeLenCodes_DecodeHuffmanStream(inStream, root, litCodeLengthCount, litLengths);
    PrintDebugText("\n\nProcessing Distance Codes...\n\n");
    u32 distCodeLengthCount = HDIST + 1;
    u32 *distLengths = malloc(sizeof(u32) * distCodeLengthCount);
    CodeLenCodes_DecodeHuffmanStream(inStream, root, distCodeLengthCount, distLengths);

    mem_pool litDistLenPool = Pool_Create(huff_tree_node, ((1 << 16) - 1) + ((1 << 16) - 1));
    PrintDebugText("\n\nCreating LitLen Huffman Codes\n\n");
    huff_tree_node *litLenRoot = CreateHuffCodesLitLenDist(litLengths, litCodeLengthCount, &litDistLenPool);
    PrintDebugText("\n\nCreating Distance Huffman Codes\n\n");
    huff_tree_node *distRoot = CreateHuffCodesLitLenDist(distLengths, distCodeLengthCount, &litDistLenPool);

    PrintDebugText("\n\nDecoding Compressed Data...\n\n");

    while(true)
    {
        huff_tree_node *node = litLenRoot;
        while(node != NULL && node->type != NodeType_Leaf)
        {
            u8 nextBit = EatBitsLSB(inStream, 1);
            PrintDebugF("%u", nextBit);
            node = node->children[nextBit];
        }

        tbs_assert(node != NULL);
        if(node->value < 256)
        {
            PrintDebugF(", Lit Value: %u\n", node->value);
            *outStream->offset = node->value;
            ++outStream->offset;
        }
        else if(node->value > 256)
        {
            u32 baseValue = lengthLookup[node->value-257].baseValue;
            u32 offsetValue = 0;
            u32 len = 0;
            for(u32 ei = 0; ei < lengthLookup[node->value-257].extraBits; ++ei)
            {
                u8 bit = EatBitsLSB(inStream, 1);
                offsetValue |= (bit << ei);
            }

            PrintDebugF(", Len Value: %u, Len: %u, ", node->value, baseValue + offsetValue);
            len = baseValue + offsetValue;
            node = distRoot;
            while(node != NULL && node->type != NodeType_Leaf)
            {
                u8 nextBit = EatBitsLSB(inStream, 1);
                PrintDebugF("%u", nextBit);
                node = node->children[nextBit];
            }

            tbs_assert(node != NULL);
            tbs_assert(node->value < 32);
            baseValue = distanceLookup[node->value].baseValue;
            offsetValue = 0;
            for(u32 ei = 0; ei < distanceLookup[node->value].extraBits; ++ei)
            {
                u8 bit = EatBitsLSB(inStream, 1);
                offsetValue |= (bit << ei);
            }
            PrintDebugF(", Dist Index: %u, Dist: %u\n", node->value, baseValue + offsetValue);

            u32 dis = baseValue + offsetValue;
            for(u32 i = len; i > 0; --i)
            {
                *outStream->offset = *(outStream->offset - dis);
                ++outStream->offset;
            }
        }
        else
        {
            PrintDebugText("\n\nFound End Of Compressed stream\n\n");
            break;
        }
    }
    PrintDebugText("\n");
}


static void ProcessDeflateStream(bit_stream *compressStream, byte_stream *result)
{
    printf("Processing deflate stream...\n\n");
    u32 BFINAL = 0;
    do
    {
        printf("====Block Start====\n");
        // TODO(torgrim): This doesn't need to be its own function.
        BFINAL = EatBitsLSB(compressStream, 1);
        u32 BTYPE = EatBitsLSB(compressStream, 2);
        if(BFINAL)
        {
            printf("!!!!Is Final Block!!!!\n");
        }
        PrintBits("BFINAL", BFINAL, 1);
        PrintBits("BTYPE", BTYPE, 2);

        printf("BTYPE: %u\n", BTYPE);
        printf("Bit Index: %hhu\n", compressStream->bitIndex);
#if 1
        switch(BTYPE)
        {
            case DeflateBlockType_NoCompress:
                {
                    PrintDebugText("BlockType No Compression\n");
                    if(compressStream->bitIndex > 0)
                    {
                        compressStream->bitIndex = 0;
                        ++compressStream->offset;
                    }
                    ProcessNonCompressedBlock(compressStream, result);
                }break;
            case DeflateBlockType_Fixed:
                {
                    PrintDebugText("BlockType Fixed Huffman\n");
                    DecompressFixedHuffman(compressStream, result);
                }break;
            case DeflateBlockType_Dynamic:
                {
                    PrintDebugText("BlockType Dynamic Huffman\n");
                    DecompressDynamicHuffman(compressStream, result);
                }break;
            default:
                {
                    printf("HEADER_ERROR::Unknown BlockType\n");
                    printf("Current Output Size: %zu\n", result->offset - result->base);
                    tbs_assert(false);
                }
        }
#endif
    } while(BFINAL != 1);

    i64 resultSize = result->offset - result->base;
    printf("Output Size: %zd\n", resultSize);
#if 0
    printf("Output Bits:\n");
    for(u32 i = 0; i < resultSize; ++i)
    {
        u8 value = *(result.base + i);
        PrintBits("", value, 8);
    }
#endif
}

static ihdr_chunk ProcessDataIHDR(chunk_header *chunk)
{
    ihdr_chunk result = {0};
    result.width = Chunk_GetU32AndAdvance(chunk);
    result.height = Chunk_GetU32AndAdvance(chunk);
    result.bitDepth = Chunk_GetI8AndAdvance(chunk);
    result.colorType = Chunk_GetI8AndAdvance(chunk);
    result.compressMethod = Chunk_GetI8AndAdvance(chunk);
    result.filterMethod = Chunk_GetI8AndAdvance(chunk);
    result.interlaceMethod = Chunk_GetI8AndAdvance(chunk);

    result.width = BigToLittleEndianU32(result.width);
    result.height = BigToLittleEndianU32(result.height);

    printf("IHDR width: %u\n", result.width);
    printf("IHDR height: %u\n", result.height);
    printf("IHDR bit depth: %hhd\n", result.bitDepth);
    printf("IHDR color type: %hhd\n", result.colorType);
    printf("IHDR compress method: %hhd\n", result.compressMethod);
    printf("IHDR filter method: %hhd\n", result.filterMethod);
    printf("IHDR interlace method: %hhd\n", result.interlaceMethod);

    return result;
}

static void ProcessIDATList(chunk_header *headerList, u32 headerCount, byte_stream *outStream)
{
    tbs_assert(headerCount > 0);
    chunk_header firstHeader = headerList[0];
    printf("Processing IDAT data...\n");
    printf("Chunk Size: %llu\n", firstHeader.data.size);
    tbs_assert(firstHeader.type == ChunkType_IDAT);
    byte_stream data = firstHeader.data;
    u8 zlib_CompressionAndFlags = ByteStream_GetU8AndAdvance(&data);
    u8 CM = zlib_CompressionAndFlags & 0xf;
    u8 CINFO = zlib_CompressionAndFlags >> 4;
    printf("CINFO: %hhu\n", CINFO);
    printf("CM: %hhu\n", CM);
    u8 zlib_AdditionalFlags = ByteStream_GetU8AndAdvance(&data);
    u8 FCHECK = (zlib_AdditionalFlags & 0x1f);
    u8 FDICT = (zlib_AdditionalFlags & 32);
    u8 FLEVEL = (zlib_AdditionalFlags >> 6);
    printf("FCHECK: %hhu\n", FCHECK);
    printf("FDICT: %hhu\n", FDICT);
    printf("FLEVEL: %hhu\n", FLEVEL);
    printf("FCHECK Verify: %d\n", (zlib_CompressionAndFlags * 256 + zlib_AdditionalFlags) % 31);

    bool checkValid = ((zlib_CompressionAndFlags * 256 + zlib_AdditionalFlags) % 31 == 0);

    if(CM == 8 && checkValid == true && (CINFO == 0 || CINFO == 7))
    {
        // NOTE(torgrim): 2 = CM + CINFO
        u32 compressSize = firstHeader.data.size - 2;
        for(u32 i = 1; i < headerCount; ++i)
        {
            compressSize += headerList[i].data.size;
        }

        bit_stream compressData = {0};
        compressData.base = malloc(compressSize);
        compressData.offset = compressData.base;
        memcpy(compressData.offset, data.offset, firstHeader.data.size - 2);
        compressData.offset += (firstHeader.data.size - 2);
        for(u32 i = 1; i < headerCount; ++i)
        {
            if(i == headerCount - 1)
            {
                memcpy(compressData.offset, headerList[i].data.base, headerList[i].data.size - 4);
                compressData.offset += headerList[i].data.size - 4;
            }
            else
            {
                memcpy(compressData.offset, headerList[i].data.base, headerList[i].data.size);
                compressData.offset += headerList[i].data.size;
            }
        }

        compressData.offset = compressData.base;
        ProcessDeflateStream(&compressData, outStream);
    }
    else
    {
        printf("Invalid Compression Method found Or Invalid Check Value\n");
        tbs_assert(false);
    }

    chunk_header lastHeader = headerList[headerCount-1];
    u32 zlib_CheckValue = *(u32 *)(lastHeader.data.base+(lastHeader.data.size - 4));
}

#if defined(_WIN64)
static char *GetCWD(void)
{
    char *result = NULL;
    DWORD neededSize = GetCurrentDirectory(0, NULL);
    tbs_assert(neededSize > 0);

    char *buffer = malloc(sizeof(char) * neededSize);
    if(GetCurrentDirectory(neededSize, buffer))
    {
        result = buffer;
    }
    return result;
}

static file_data ReadAllFile(char *filename)
{
    file_data result = {0};
    HANDLE fHandle = CreateFileA(filename, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    if(fHandle != INVALID_HANDLE_VALUE)
    {
        LARGE_INTEGER fileSize;
        if(GetFileSizeEx(fHandle, &fileSize))
        {
            if(fileSize.LowPart > 0)
            {
                u8 *buffer = malloc(sizeof(u8) * fileSize.LowPart);
                DWORD bytesRead = 0;
                if(ReadFile(fHandle, buffer, fileSize.LowPart, &bytesRead, NULL))
                {
                    result.data = buffer;
                    result.size = fileSize.LowPart;
                }
                else
                {
                    PrintTextF("FILE_IO_ERROR:: Could not read file\n");
                }
            }
        }
        else
        {
            PrintTextF("FILE_IO_ERROR:: Could not get file size\n");
        }

        CloseHandle(fHandle);
    }
    else
    {
        PrintTextF("FILE_IO_ERROR:: Could not create file\n");
    }

    return result;
}

static bool WriteAllFile(char *filename, u8 *data, u64 size)
{
    bool result = false;
    HANDLE fHandle = CreateFileA(filename, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
    if(fHandle != INVALID_HANDLE_VALUE)
    {
        DWORD bytesWritten = 0;
        result = WriteFile(fHandle, data, size, &bytesWritten, NULL);
        tbs_assert(bytesWritten == size);

        CloseHandle(fHandle);
    }

    return result;
}
#elif defined(__linux__)
static char *GetCWD()
{
    char *result = getcwd(NULL, 0);
    tbs_assert(result != NULL);
    return result;
}

static file_data ReadAllFile(char *filename)
{
    file_data result = {0};
    int openFlags = O_RDONLY;
    int fHandle = open(filename, openFlags);
    if(fHandle != -1)
    {
        file_stat fInfo;
        int succ = fstat(fHandle, &fInfo);
        if(succ != -1)
        {
            u8 *buffer = malloc(sizeof(u8) * fInfo.st_size);
            ssize_t readSize = read(fHandle, buffer, fInfo.st_size);
            if(readSize > -1 && readSize == fInfo.st_size)
            {
                result.data = buffer;
                result.size = (u64)readSize;
            }
            else
            {
                PrintTextF("FILE_IO_ERROR:: Could not read file\n");
            }
        }

        close(fHandle);
    }
    else
    {
        PrintTextF("FILE_IO_ERROR:: Could not create file\n");
    }

    return result;
}

static bool WriteAllFile(char *filename, u8 *data, u64 size)
{
    bool result = false;
    int fHandle = open(filename, O_WRONLY|O_CREAT|O_TRUNC, S_IRUSR|S_IWUSR);
    if(fHandle > -1)
    {
        ssize_t bytesWritten = write(fHandle, data, size);
        if(bytesWritten == (ssize_t)size)
        {
            result = true;
        }

        close(fHandle);
    }

    return result;
}

#endif


static bool appRunning = true;

static LRESULT CALLBACK MainWindowCallback(HWND window, UINT msg, WPARAM wParam, LPARAM lParam)
{
    LRESULT result = 0;
    switch(msg)
    {
        case WM_QUIT:
        case WM_CLOSE:
        {
            appRunning = false;
        }break;
        case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC dc = BeginPaint(window, &ps);
            RECT clientRect;
            GetClientRect(window, &clientRect);
            FillRect(dc, &clientRect, CreateSolidBrush(RGB(10, 10, 10)));
            EndPaint(window, &ps);
        }
    }
    result = DefWindowProc(window, msg, wParam, lParam);
    return result;
}

static HWND CreatePreviewWindow(void)
{
    HWND windowHandle = NULL;
    HMODULE appInstance = GetModuleHandleA(NULL);
    WNDCLASSA winClass = {0};
    winClass.style = CS_OWNDC;
    winClass.lpfnWndProc = MainWindowCallback;
    winClass.hInstance = appInstance;
    winClass.lpszClassName = "png_loader";

    if(RegisterClassA(&winClass))
    {
        windowHandle = CreateWindowExA(0, winClass.lpszClassName, "PNG Decoder", WS_OVERLAPPEDWINDOW | WS_VISIBLE,
                                       CW_USEDEFAULT, CW_USEDEFAULT, 1920, 1080,
                                       NULL, NULL, appInstance, NULL);
        if(windowHandle != NULL)
        {
            printf("Main Window Created\n");
        }
        else
        {
            printf("WIN32 ERROR:: Could not create main window\n");
        }
    }

    return windowHandle;
}

static i32 PaethPredictor(i32 a, i32 b, i32 c)
{
    tbs_assert(a >= 0 && b >= 0 && c >= 0);
    i32 p = a+b-c;
    i32 pa = abs(p-a);
    i32 pb = abs(p-b);
    i32 pc = abs(p-c);
    if(pa <= pb && pa <= pc)
    {
        return a;
    }
    else if(pb <= pc)
    {
        return b;
    }
    else
    {
        return c;
    }
}

int main(int argc, char *argv[])
{
    if(argc <= 1)
    {
        printf("\nNo input file given... exiting.\n");
        return 0;
    }

    char *filename = argv[1];
    file_data inputFile = ReadAllFile(filename);
    //file_data inputFile = ReadAllFile("../test_images/pixel_block.png");
    //file_data inputFile = ReadAllFile("../test_images/test.png");
    //file_data inputFile = ReadAllFile("../test_images/big_test.png");
    //file_data inputFile = ReadAllFile("../test_images/adventure.png");
    //file_data inputFile = ReadAllFile("../test_images/ubuntu.png");
    //file_data inputFile = ReadAllFile("../test_images/bladerunner.png");
    if(inputFile.size > 0)
    {
        byte_stream stream = {.base = inputFile.data, .offset = inputFile.data, .size = inputFile.size};
        PrintByteArray("Signature Bytes", stream.base, 8);
        stream.offset += 8;
        PrintChunkHeaderByteStream(stream.base + 8);

        printf("Processing IHDR chunk...\n");
        chunk_header headerIHDR = Stream_GetNextChunkAndAdvance(&stream);
        printf("Chunk Size: %lld\n", headerIHDR.data.size);

        tbs_assert(headerIHDR.type == ChunkType_IHDR);
        tbs_assert(headerIHDR.data.size == 13);

        ihdr_chunk dataIHDR = ProcessDataIHDR(&headerIHDR);
        tbs_assert(dataIHDR.colorType == 2 || dataIHDR.colorType == 6);
        size_t bytesPerPixel = 4;
        if(dataIHDR.colorType == 2)
        {
            bytesPerPixel = 3;
        }
        byte_stream resultStream = {0};
        resultStream.base = malloc(dataIHDR.width * dataIHDR.height * bytesPerPixel + dataIHDR.height);
        resultStream.offset = resultStream.base;
        memset(resultStream.base, 0, dataIHDR.width * dataIHDR.height * bytesPerPixel);

        chunk_header idatChunks[100] = {0};
        u32 idatCount = 0;

        while(stream.offset < stream.base + stream.size)
        {
            chunk_header nextChunk = Stream_GetNextChunkAndAdvance(&stream);
            switch(nextChunk.type)
            {
                case ChunkType_PLTE:
                    {
                        //ProcessDataPLTE(&nextChunk);
                        printf("Processing PLTE chunk...\n");
                    }break;
                case ChunkType_IDAT:
                    {
                        idatChunks[idatCount++] = nextChunk;
                        //ProcessDataIDAT(&nextChunk, &resultStream);
                    }break;
                default:
                    {
                    }
            }
        }

        tbs_assert(idatCount > 0);
        ProcessIDATList(idatChunks, idatCount, &resultStream);

        HWND windowHandle = CreatePreviewWindow();
        if(windowHandle)
        {
            LONG w = dataIHDR.width;
            LONG h = dataIHDR.height;
            BITMAPINFOHEADER bmiHeader = {0};
            bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
            bmiHeader.biWidth = w;
            bmiHeader.biHeight = -h;
            bmiHeader.biPlanes = 1;
            bmiHeader.biBitCount = 32;
            bmiHeader.biCompression = BI_RGB;

            BITMAPINFO bmi = {0};
            bmi.bmiHeader = bmiHeader;

            HDC deviceContext = GetDC(windowHandle);
            u8 *pixels = malloc(w*h*4);
            memset(pixels, 0, w*h*bytesPerPixel);
            u8 *color = pixels;
            u8 *imageBytes = resultStream.base;
            tbs_assert(w != 0);
            tbs_assert(h != 0);
            // TODO(torgrim): Currently scanning in the wrong y direction since png is top to bottom
            for(u32 y = 0; y < h; ++y)
            {
                u8 filter = *imageBytes++;
                printf("Scanline filter type: %hhu\n", filter);

                if(filter == 0)
                {
                    for(u32 x = 0; x < w; ++x)
                    {
                        i32 b = imageBytes[2];
                        i32 g = imageBytes[1];
                        i32 r = imageBytes[0];
                        color[0] = b;
                        color[1] = g;
                        color[2] = r;
                        color[3] = 0;

                        color += 4;
                        imageBytes += bytesPerPixel;
                    }
                }
                else if(filter == 1)
                {
                    u8 *scanline = color;
                    // NOTE(torgrim): Special case for first pixel.
                    color[0] = imageBytes[2];
                    color[1] = imageBytes[1];
                    color[2] = imageBytes[0];
                    color[3] = 0;

                    color += 4;
                    imageBytes += bytesPerPixel;

                    for(u32 x = 1; x < w; ++x)
                    {
                        u32 xOffset = x-1;
#if 1
                        i32 b = imageBytes[2] + (i32)scanline[(xOffset*4) + 0];
                        i32 g = imageBytes[1] + (i32)scanline[(xOffset*4) + 1];
                        i32 r = imageBytes[0] + (i32)scanline[(xOffset*4) + 2];
                        color[0] = b % 256;
                        color[1] = g % 256;
                        color[2] = r % 256;
                        color[3] = 0;
#elif 1
                        i32 b = imageBytes[2] + (i32)scanline[(xOffset*4) + 0];
                        i32 g = imageBytes[1] + (i32)scanline[(xOffset*4) + 1];
                        i32 r = imageBytes[0] + (i32)scanline[(xOffset*4) + 2];
                        color[0] = b;
                        color[1] = g;
                        color[2] = r;
                        color[3] = 0;
#elif 1
                        color[0] = 255;
                        color[1] = 0;
                        color[2] = 255;
                        color[3] = 0;
#endif

                        color += 4;
                        imageBytes += bytesPerPixel;
                    }
                }
                else if(filter == 2)
                {
                    tbs_assert(y > 0);
                    u8 *scanline = pixels + ((y-1)*(w*4));
                    for(u32 x = 0; x < w; ++x)
                    {
                        i32 b = (imageBytes[2] + (i32)scanline[(x*4) + 0]);
                        i32 g = (imageBytes[1] + (i32)scanline[(x*4) + 1]);
                        i32 r = (imageBytes[0] + (i32)scanline[(x*4) + 2]);
#if 1
                        color[0] = b % 256;
                        color[1] = g % 256;
                        color[2] = r % 256;
                        color[3] = 0;
#elif 1
                        color[0] = b;
                        color[1] = g;
                        color[2] = r;
                        color[3] = 0;
#elif 1
                        color[0] = 0;
                        color[1] = 0;
                        color[2] = 255;
                        color[3] = 0;
#endif

                        color += 4;
                        imageBytes += bytesPerPixel;
                    }
                }
                else if(filter == 3)
                {
                    tbs_assert(y > 0);
                    u8 *prior = pixels + ((y-1)*(w*4));
                    u8 *raw = color;

                    color[0] = (imageBytes[2] + (i32)floor(prior[0] / 2)) % 256;
                    color[1] = (imageBytes[1] + (i32)floor(prior[1] / 2)) % 256;
                    color[2] = (imageBytes[0] + (i32)floor(prior[2] / 2)) % 256;
                    color[3] = 0;

                    color += 4;
                    imageBytes += bytesPerPixel;
                    for(u32 x = 1; x < w; ++x)
                    {
                        u32 xPrior = (x-1)*4;
                        u32 xOffset = x*4;
#if 1
                        i32 b = (raw[xPrior] + prior[xOffset]);
                        i32 g = (raw[xPrior+1] + prior[xOffset+1]);
                        i32 r = (raw[xPrior+2] + prior[xOffset+2]);
                        //tbs_assert(r <= 255);
                        //tbs_assert(g <= 255);
                        //tbs_assert(b <= 255);
                        color[0] = (imageBytes[2] + (i32)floor(b / 2)) % 256;
                        color[1] = (imageBytes[1] + (i32)floor(g / 2)) % 256;
                        color[2] = (imageBytes[0] + (i32)floor(r / 2)) % 256;
                        color[3] = 0;
#elif 1
                        color[0] = imageBytes[2] + floor((raw[xPrior] + prior[xOffset]) / 2);
                        color[1] = imageBytes[1] + floor((raw[xPrior+1] + prior[xOffset+1]) / 2);
                        color[2] = imageBytes[0] + floor((raw[xPrior+2] + prior[xOffset+2]) / 2);
                        color[3] = 0;
#elif 1
                        color[0] = 255;
                        color[1] = 0;
                        color[2] = 0;
                        color[3] = 0;
#endif

                        color += 4;
                        imageBytes += bytesPerPixel;
                    }
                }
                else if(filter == 4)
                {
                    tbs_assert(y > 0);
                    u8 *prior = pixels + ((y-1)*(w*4));
                    u8 *raw = color;

                    color[0] = imageBytes[2] + PaethPredictor(0, prior[0], 0) % 256;
                    color[1] = imageBytes[1] + PaethPredictor(0, prior[1], 0) % 256;
                    color[2] = imageBytes[0] + PaethPredictor(0, prior[2], 0) % 256;
                    color[3] = 0;

                    color += 4;
                    imageBytes += bytesPerPixel;
                    for(u32 x = 1; x < w; ++x)
                    {
                        u32 xPrior = (x-1)*4;
                        u32 xOffset = x*4;
#if 1
                        color[0] = (imageBytes[2] + PaethPredictor(raw[xPrior], prior[xOffset], prior[xPrior])) % 256;
                        color[1] = (imageBytes[1] + PaethPredictor(raw[xPrior+1], prior[xOffset+1], prior[xPrior+1])) % 256;
                        color[2] = (imageBytes[0] + PaethPredictor(raw[xPrior+2], prior[xOffset+2], prior[xPrior+2])) % 256;
                        color[3] = 0;
#elif 1
                        color[0] = (imageBytes[2] + PaethPredictor(raw[xPrior], prior[xOffset], prior[xPrior]));
                        color[1] = (imageBytes[1] + PaethPredictor(raw[xPrior+1], prior[xOffset+1], prior[xPrior+1]));
                        color[2] = (imageBytes[0] + PaethPredictor(raw[xPrior+2], prior[xOffset+2], prior[xPrior+2]));
                        color[3] = 0;
#elif 1
                        color[0] = 0;
                        color[1] = 255;
                        color[2] = 0;
                        color[3] = 0;
#endif

                        color += 4;
                        imageBytes += bytesPerPixel;
                    }
                }
                else
                {
                    printf("Unsupported filter type: %hhu\n", filter);
                    tbs_assert(false);
                }
            }

            while(appRunning)
            {
                MSG msg;
                while(PeekMessageA(&msg, windowHandle, 0, 0, PM_REMOVE))
                {
                    TranslateMessage(&msg);
                    DispatchMessage(&msg);
                }

                StretchDIBits(deviceContext, 0, 0, w, h, 0, 0, w, h, pixels, &bmi, DIB_RGB_COLORS, SRCCOPY);
            }
        }
    }
    else
    {
        printf("Could not open file\n");
    }
    return 0;
}
