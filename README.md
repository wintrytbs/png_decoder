# PNG Decoder

A basic PNG decoder built in C. Currently only supports Windows.
This is an educational project used as an introduction to image compression and image formats.
It isn't suitable to be used in other projects as it is not anywhere near a professional grade
decoder.

### Building

To build the project run the **build.bat** file located in the **code** directory.
The default compiler is MSVC cl.exe so it needs to be available on the command line.
```
build.bat
```
Clang can be invoked by providing **clang** as an argument to the build file.
```
build.bat clang
```
